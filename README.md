# neo4j-cypher-dev

Docker Compose for Neo4j Community

## Getting started

We need some tools to serve the Neo4j Dashboard

- Docker
- Docker Compose

You can run this to create the image and the container

`docker-compose up --build -d`
